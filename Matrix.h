/*

	Class to handle matricies.
	Like in MATLAB, and in real math, elements are indexed beginning at 1, not 0.

*/

#include <iostream>
#include <vector>
using namespace std;

class Matrix {

	private:
		vector<vector<double*>> x;

	public:
		Matrix();										
			// makes a matrix with 0 rows and 0 columns
		Matrix(int n);									
			// makes identity matrix of n diagonal size
		Matrix(int m, int n);							
			// makes zero matrix of m rows and n columns
		Matrix size();
			// returns the size of a matrix as a matrix where 
			// size = [rows, columns] (1D matrix)
		int sizeRow();
			// returns the vertical size of the matrix
		int sizeCol();
			// returns the horizontal size of the matrix
		Matrix static hConcat(Matrix& a, Matrix& b);
			// returns the horizontal combination of two matricies 
			// (assuming they are compatable)
		Matrix static vConcat(Matrix& a, Matrix& b);
			// returns the vertical combination of two matricies 
			// (assuming they are compatable)
		Matrix getRow(int n);
			// returns refrences to all elements of the row indexed at n (1D matrix)
		Matrix getCol(int n);
			// returns references all elements of the column indexed at n (1D matrix)
		void setRow(Matrix& a, int row);
			// sets this matrix's row to the a matrix (a needs to be a 1D matrix of
			// the same number of columns as the original matrix
			// used to be a functional replacement of a.getRow(x) = rowMatrix
		Matrix static id(int n);						
			// returns identity matrix of n diagonal size
		Matrix static zero(int m, int n);				
			// returns matrix of m rows and n columns of zeros
		Matrix static one(int m, int n);				
			// same as zero, but fills with ones rather than zeros
		Matrix static dotMult(Matrix& a, Matrix& b);	
			// perform dot multiplication (each-by-every)
		double static dotProd(Matrix& a, Matrix& b);	
			// returns the dot product of the two matricies (scalar value)
		Matrix static crossMult(Matrix& a, Matrix& b);	
			// returns cross product of two matricies
		Matrix static gaussJordan(Matrix& a, Matrix& b);
			// performs Gauss-Jordan Elimination and returns the result where
			// a is the coefficient matrix and b is the solution matrix
			// returns vertical matrix of solutions (x values)
		
	friend ostream& operator << (ostream& outputStream, Matrix& n) {
		
		cout << endl << "[";

		for (int i = 0; i < (long)n.x.size(); i++) {
			cout << "\t";
			for (int j = 0; j < (long)n.x[i].size(); j++) {
				cout << *n.x[i][j] << "\t";
			}
			if (i != (long)n.x.size() - 1) {
				cout << endl;
			}
		}

		cout << "]" << endl;

		return outputStream;

	}

	friend Matrix operator + (Matrix& a, Matrix& b) {

		Matrix *c = new Matrix((int)a.x.size(), (int)a.x[0].size());
		
		if ((long)a.x.size() == (long)b.x.size()) {
			
			for (int i = 0; i < (long)a.x.size(); i++ ) {
				
				if ((long)a.x[i].size() == (long)b.x[i].size()) {
					
					for (int j = 0; j < (long)a.x[i].size(); j++) {
						*c -> x[i][j] = *a.x[i][j] + *b.x[i][j];
					}

				} else {
					// throw exception
					cout << "Error, matrix dimensions must match" << endl;
				}

			}

		} else {
			// throw exception as matricies must be the same size to be added
			cout << "Error, matrix dimensions must match" << endl;
		}

		return *c;

	}

	friend Matrix operator - (Matrix& a, Matrix& b) {

		Matrix *c = new Matrix((int)a.x.size(), (int)a.x[0].size());
		
		if ((long)a.x.size() == (long)b.x.size()) {
			
			for (int i = 0; i < (long)a.x.size(); i++ ) {
				
				if ((long)a.x[i].size() == (long)b.x[i].size()) {
					
					for (int j = 0; j < (long)a.x[i].size(); j++) {
						*c -> x[i][j] = *b.x[i][j] - *a.x[i][j];
					}

				} else {
					// throw exception
					cout << "Error, matrix dimensions must match" << endl;
				}

			}

		} else {
			// throw exception as matricies must be the same size to be added
			cout << "Error, matrix dimensions must match" << endl;
		}

		return *c;

	}

	friend Matrix operator * (Matrix& a, Matrix& b) {
		
		Matrix aSize = a.size();
		Matrix bSize = b.size();

		long rowA = (long)aSize(1, 1);
		long colA = (long)aSize(1, 2);
		long rowB = (long)bSize(1, 1);
		long colB = (long)bSize(1, 2);

		Matrix c = *new Matrix(rowA, colB);

		if (colA == rowB) {
			for (int i = 1; i <= rowA; i++) {
				for (int j = 1; j <= colB; j++) {
					for (int k = 1; k <= colA; k++) {
						c(i, j) += (a(i, k) * b(k, j));
					}
				}
			}
		} else {
			// throw error
			cout << "Error: inner dimensions must agree." << endl;
		}

		return c;

	}

	friend Matrix operator * (Matrix& a, double b) {
		
		Matrix aSize = a.size();

		long rowA = (long)aSize(1, 1);
		long colA = (long)aSize(1, 2);

		Matrix c = *new Matrix(rowA, colA);

		for (int i = 1; i <= rowA; i++) {
			for (int j = 1; j <= colA; j++) {
				c(i, j) = a(i, j) * b;
			}
		}

		return c;

	}

	friend Matrix operator / (Matrix& a, double b) {
		
		Matrix aSize = a.size();

		long rowA = (long)aSize(1, 1);
		long colA = (long)aSize(1, 2);

		Matrix c = a;

		for (int i = 1; i <= rowA; i++) {
			//cout << "i=" << i << endl;
			for (int j = 1; j <= colA; j++) {
				//cout << "j=" << j << endl;
				//cout << "b=" << b << endl;
				//cout << c(i, j) << " / " << b << " = " << (c(i, j) / b) << endl;
				c(i, j) /= b;
			}
		}

		return c;

	}

	double& Matrix::operator() (const int m, const int n) {
		
		/*Matrix size = this -> size();
		int row = size(1, 1);
		int col = size(1, 2);
		double val = 0;

		if (m <= row && n <= col) {
			val = *this -> x[m - 1][n - 1];
		} 

		return val;*/

		return *this -> x[m - 1][n - 1];

	}

	/*double& Matrix::operator[] (const int n) {
		
		int row = this -> sizeRow();
		int col = this -> sizeCol();
		int total = row * col;
		
		if (n <= total) {
			return this -> x[(total % n) - 1][];
		}

		return;

	}*/

};

Matrix::Matrix() {
	//this -> x.push_back(*new vector<double>);
	//this -> x[0].push_back(0);
}

Matrix::Matrix(int n) {
	
	for (int i = 0; i < n; i++) {
		this -> x.push_back(*new vector<double*>);
		for (int j = 0; j < n; j++) {
			if (i == j) {
				this -> x [i].push_back(new double(1));
			} else {
				this -> x [i].push_back(new double(0));
			}
		}
	}

}

Matrix::Matrix(int m, int n) {
	
	for (int i = 0; i < m; i++) {
		this -> x.push_back(*(new vector<double*>));
		for (int j = 0; j < n; j++) {
			this -> x [i].push_back(new double(0));
		}
	}

}

Matrix Matrix::size() {
	
	Matrix size = *new Matrix(1, 2);
	int currMax = 0;

	/*for (vector<double*> current : this -> x) {
		if ((long)current.size() > currMax) {
			currMax = (long)current.size();
		}
	}*/

	size(1, 1) = (double)this -> x.size();
	//size(1, 2) = currMax;
	size(1, 2) = (double)this -> x[0].size();

	return size;

}

int Matrix::sizeRow() {
	return (int)this -> x.size();
}

int Matrix::sizeCol() {
	return (int)this -> x[0].size();
}

Matrix Matrix::hConcat(Matrix& a, Matrix& b) {
	
	Matrix aSize = a.size();
	Matrix bSize = b.size();

	Matrix c = *new Matrix((long)aSize(1, 1), (long)aSize(1, 2) + (long)bSize(1, 2));
	Matrix cSize = c.size();

	// check to make sure matricies are compatible (same row dimension)
	if (aSize(1, 1) == bSize(1, 1)) {

		for (int i = 1; i <= cSize(1, 1); i++) {
			for (int j = 1; j <= cSize(1, 2); j++) {
				if (j <= aSize(1, 2)) {
					c(i, j) = a(i, j);
				} else {
					c(i, j) = b(i, j - (int)aSize(1, 2));
				}
			}
		}

	} else {
		cout << "Error, row dimensions must match." << endl;
	}

	return c;

}

Matrix Matrix::vConcat(Matrix& a, Matrix& b) {
	
	Matrix aSize = a.size();
	Matrix bSize = b.size();

	Matrix c = *new Matrix((long)aSize(1, 1) + (long)bSize(1, 1), (long)aSize(1, 2));
	Matrix cSize = c.size();

	// check to make sure matricies are compatible (same row dimension)
	if (aSize(1, 2) == bSize(1, 2)) {

		for (int i = 1; i <= cSize(1, 1); i++) {
			for (int j = 1; j <= cSize(1, 2); j++) {
				if (j <= aSize(1, 2)) {
					c(i, j) = a(i, j);
				} else {
					c(i, j) = b(i, j - (int)aSize(1, 2));
				}
			}
		}

	} else {
		cout << "Error, column dimensions must match." << endl;
	}

	return c;

}

Matrix Matrix::getRow(int n) {

	// return a 1D matrix where each element is a reference to the 
	// corresponding row in the passed matrix object

	Matrix size = this -> size();
	int row = (int)size(1, 1);
	int col = (int)size(1, 2);
	Matrix c = *new Matrix();

	// check to make sure the parameter is within the bounds of the matrix
	if (n <= row) {
		c.x.push_back(this -> x[n - 1]);
	} else {
		// out of bounds exception
		cout << "Out of bounds: " << n << " is not a row within the bounds of this matrix." << endl;
	}

	return c;

}

Matrix Matrix::getCol(int n) {

	// return a 1D matrix where each element is a reference to the 
	// corresponding row in the passed matrix object

	Matrix size = this -> size();
	int row = (int)size(1, 1);
	int col = (int)size(1, 2);
	Matrix c = *new Matrix();

	// check to make sure the parameter is within the bounds of the matrix
	if (n <= col) {
		for (int i = 0; i < row; i++) {
			c.x.push_back(*new vector<double*>);
			//cout << "i: " << i << " | " << *this -> x[i][n - 1] << endl;
			c.x[i].push_back(this -> x[i][n - 1]);
		}
	} else {
		// out of bounds exception
		cout << "Out of bounds: " << n << " is not a column within the bounds of this matrix." << endl;
	}

	//cout << "getCol done" << endl;

	return c;

}

void Matrix::setRow(Matrix& a, int row) {

	Matrix sizeT = this -> size();
	Matrix sizeA = a.size();
	int rowT = (int)sizeT(1, 1);
	int colT = (int)sizeT(1, 2);
	int rowA = (int)sizeA(1, 1);
	int colA = (int)sizeA(1, 2);

	//cout << "set a: " << a << endl << "set this: " << *this << endl;
	
	if (colT == colA && rowA == 1) {
		for (int j = 1; j <= colT; j++) {
			//cout << j << endl;
			*this -> x[row-1][j-1] = a(1, j);
		}
	} else {
		cout << "Error setting row" << endl;
	}

}

Matrix Matrix::id(int n) {
	
	Matrix *a = new Matrix();

	for (int i = 0; i < n; i++) {
		a -> x.push_back(*new vector<double*>);
		for (int j = 0; j < n; j++) {
			if (i == j) {
				a -> x [i].push_back(new double(1));
			} else {
				a -> x [i].push_back(new double(0));
			}
		}
	}

	return *a;

}

Matrix Matrix::zero(int m, int n) {
	return *new Matrix(m, n);
}

Matrix Matrix::one(int m, int n) {
	
	Matrix *a = new Matrix(m, n);

	for (int i = 0; i < m; i++) {
		for (int j = 0; j < n; j++) {
			a -> x [i][j] = new double(1);
		}
	}

	return *a;

}

Matrix Matrix::gaussJordan(Matrix& a, Matrix&b) {

	Matrix c = hConcat(a, b);
	Matrix cSize = c.size();
	int row = (int)cSize(1, 1);
	int col = (int)cSize(1, 2);

	for (int i = 1; i <= row; i++) {
		c.setRow(c.getRow(i) / c(i, i), i);
		for (int j = 1; j <= row; j++) {
			if (i != j) {
				//c.getRow(j) = c.getRow(j) + c.getRow(i) * -c(j, i);
				c.setRow(c.getRow(j) + c.getRow(i) * -c(j, i), j);
			}
		}
	}

	return c.getCol(col);

}