#include <iostream>
#include "Matrix.h"
using namespace std;

int main() {
	
	Matrix a = Matrix::id(3);
	Matrix b = Matrix::one(3, 1);
	Matrix c = *new Matrix(3, 3);
	Matrix d = *new Matrix(3, 1);

	c(1, 1) = 4;
	c(1, 2) = 5;
	c(1, 3) = -2;
	c(2, 1) = 2;
	c(2, 2) = -5;
	c(2, 3) = 2;
	c(3, 1) = 6;
	c(3, 2) = 2;
	c(3, 3) = 4;

	d(1, 1) = -6;
	d(2, 1) = 24;
	d(3, 1) = 30;

	a(1, 2) = 3;

	cout << "a = " << a << endl;
	cout << "b = " << b << endl;
	cout << "c = " << c << endl;
	cout << "d = " << d << endl;

	cout << "a + b = " << (a + b) << endl;
	cout << "a + c = " << (a + c) << endl;
	cout << "a * b = " << (a * b) << endl;
	cout << "a * c = " << (a * c) << endl;

	cout << "Gauss-Jordan Elimination (c, d): " << Matrix::gaussJordan(c, d) << endl;
	
	return 0;

}